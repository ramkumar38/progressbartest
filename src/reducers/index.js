import { combineReducers } from "redux";
import GetProgress from "./reducer_progress";

const rootReducer = combineReducers({
getProgress : GetProgress
});

export default rootReducer;