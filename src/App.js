import React from 'react';
import logo from './logo.svg';
import './App.css';
import  DynamicControl from "./components/DynamicControl";
import NavBar from  './components/Navbar'

function App() {
  return (
    <div className="App">
      <NavBar />
     <DynamicControl />
    </div>
  );
}

export default App;
