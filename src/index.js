import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore,applyMiddleware } from "redux";
import { Provider } from "react-redux";
import routes from './route';
import { Router, browserHistory} from "react-router";
import ReduxPromise from "redux-promise";
import reducers from "./reducers";
import 'bootstrap/dist/css/bootstrap.min.css';
const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);

ReactDOM.render(
  
   <Provider store ={createStoreWithMiddleware(reducers)}>
      <Router history={browserHistory}  routes={routes} />
   </Provider>
 ,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
