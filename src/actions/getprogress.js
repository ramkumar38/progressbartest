import axio from 'axios';
import { CODE_PROGRESS } from "../helpers";

export const getProgress = () => {

    const ax = axio.create({
        baseURL : 'http://pb-api.herokuapp.com'
    });

    var progressData = ax.get('/bars');

    return {
        type:CODE_PROGRESS,
        payload:progressData
    };
}