import React,{Component} from 'react';

class NavBar extends Component{

    constructor(props){
        super(props);
    }
    render(){
        return (
                <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
                <div className="container">
                <a className="navbar-brand" href="#">Progressbar Viewer</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                    <li className="nav-item active">
                        <a className="nav-link" href="/">Home</a>
                    </li>
                   
                    
                   
                    </ul>
                </div>
                </div>
               
                </nav>
        );
    }
}

export default NavBar;