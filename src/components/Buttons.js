import React, {useEffect} from 'react';
import {Button} from '@material-ui/core'
export const Buttons = (props) => {

    const {buttons,setProFuncData,selectedIndex} = props;
    
    return (
       
        buttons.map((buttonUnit,index)=>{
        return <div><Button onClick={()=>setProFuncData(buttonUnit,selectedIndex)}  key={index} color="primary" variant="contained" >{buttonUnit}</Button></div>
        }))
    
    
}



