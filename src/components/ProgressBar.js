import React,{useEffect} from "react";

import ProgressBar from "react-bootstrap/ProgressBar";
import { CODE_DANGER,CODE_SUCCESS } from "../helpers";

export const ProgressBarControl = (props) => {

    const {bars} = props;
  
    return (
        <div>
            {bars.map((progressUnit,index)=>{ 
                return (<div> <ProgressBar max={props.limit} variant={progressUnit > props.limit ? CODE_DANGER : CODE_SUCCESS } animated now={progressUnit} key={index} label={`${progressUnit}%`} /> </div>)
            })
            }
        </div>
    )

}


