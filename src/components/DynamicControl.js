import {Buttons} from "./Buttons";
import React,{useEffect,useState} from 'react';
import {ProgressBarControl} from "./ProgressBar";
import {connect} from 'react-redux';
import '../assets/styles/progressbar.scss';
import { getProgress } from "../actions";
import { Select,InputLabel ,MenuItem} from '@material-ui/core';


 const DynamicControl = (props) => {

    const {getProgress} = props;
    const [proStateData,setProData] = useState([]);
    const [selectedIndex,setIndex] = useState(0);
    

    const setProFuncData = (data,index)=>{

       var tempData = [...proStateData];
       tempData[index] += tempData[index] + data <= 0 ? -tempData[index] : data;
       setProData(tempData); 
    }
    useEffect(()=>{
        getProgress();
         
        
    },[]);
    useEffect(()=>{
        
        const proData = typeof props.controlData !== 'undefined' ? typeof(props.controlData.bars) !== 'undefined' ? props.controlData.bars : [] : [];
        
        setProData([...proData]);     
        
    },[props.controlData]);

    const {controlData} = props;
    const buttonData = typeof controlData !== 'undefined' ? typeof(controlData.buttons) !== 'undefined' ? controlData.buttons : [] : [];
    
    
       
        return (
            <div className='parent-container'>
                <div className='progresscontainer'>
                <ProgressBarControl limit={props.controlData.limit} bars={proStateData}/>
                </div>
                <InputLabel style={{padding:'1rem'}} id="select">Select ProgressBar</InputLabel>
                <Select labelId="select"  value={selectedIndex} onChange={(e)=>{console.log(e.target.value);setIndex(e.target.value)}}>
                    {
                        proStateData.map((dunit,index)=>{
                            return (
                            <MenuItem key={index} value={index}>{`Progressbar${index + 1}`}</MenuItem>
                            )
                        })
                    }
                
                </Select>
                <div className='buttoncontainer'>
                <Buttons selectedIndex={selectedIndex} setProFuncData={setProFuncData} buttons={buttonData} />
                </div>
                
            </div>
        )
    
   
}


const mapStateToProps = (state) => {
   return { controlData : state.getProgress }
}

const mapDispatchToProps = {
getProgress
}

export default connect(mapStateToProps,mapDispatchToProps)(DynamicControl);