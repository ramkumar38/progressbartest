import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import renderer from 'react-test-renderer';
import {ProgressBarControl} from "./components/ProgressBar";
import {Buttons} from "./components/Buttons";

test('check progressbar',()=>{
const component = renderer.create(<ProgressBarControl limit={90} bars={[23,45,67]}/>);
let tree = component.toJSON();
expect(tree).toMatchSnapshot();
});

test('check progressbar',()=>{
  const component = renderer.create(<Buttons selectedIndex={0} buttons={[23,45,67]}/>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
  })


