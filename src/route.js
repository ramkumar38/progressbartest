import React from "react";
import { Router } from "react-router";

import App from "./App";

export default (<switch>
    <Router path="/" component={App}  />
</switch>)